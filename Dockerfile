# Builder stage
FROM golang:1.20-alpine AS builder

WORKDIR /ctf_bot
COPY . .

RUN go clean --modcache
RUN go mod download
RUN go build -o ctf_bot .

# Runner stage
FROM alpine:latest

ENV CTF_BOT_INSTANCES=1

WORKDIR /app
COPY --from=builder /ctf_bot/ctf_bot .
COPY --from=builder /ctf_bot/start_bot.sh .
RUN mkdir -p /app/cache

# Use bash script to start the bot
# Change argument to set number of bot instances to run
ENTRYPOINT ["sh", "-c", "./start_bot.sh $CTF_BOT_INSTANCES"]
