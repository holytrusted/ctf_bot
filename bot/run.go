package bot

import (
	"ctf_bot/authentication"
	"ctf_bot/config"
	"ctf_bot/database"
	"ctf_bot/discord"
	"ctf_bot/util"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"go.uber.org/zap"
)

var (
	loggerBasic *zap.Logger
	logger      *zap.SugaredLogger
)

// Bot object
type Bot struct {
	Config              *config.BotConfig
	LatestChallengeTime *time.Time
	AnnouncedFirstSolve []string //Challenge IDs
}

// SetLogger sets up the internal logger for the bot package
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}

// SetConfig sets up the configuration
func (bot *Bot) SetConfig(cfg *config.BotConfig) {
	cfg.Runtime.Path = os.ExpandEnv(cfg.Runtime.Path)
	bot.Config = cfg
}

// PrettyStruct prints things pretty, it should be a util and not here
func PrettyStruct(data interface{}) (string, error) {
	val, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return "", err
	}
	return string(val), nil
}

func (bot *Bot) queryReleasedChallenges(t time.Time) ([]database.Active, error) {
	const newChallengesQueryFmtStr = `query NewChallenges {
		challenges_active(
			where: {
				event: {environments: {name: {_eq: %+q}}},
				released: {_eq: true},
				release_time: {_gt: "%s"}
			}, order_by: {
				release_time: desc
			}
		) {
			event_id
			challenge {
				id
				name
				category
			}
			points
			release_time
		}
	}`

	// Generate a query string with the Latest time embedded into it
	query := fmt.Sprintf(
		newChallengesQueryFmtStr,
		bot.Config.Event.Environment,
		t.Format(time.RFC3339Nano),
	)
	responseBytes, err := database.GQLQuery(query, nil, bot.Config.Database.DefaultQueryRole)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	r := struct {
		Data struct {
			List []database.Active `json:"challenges_active"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(responseBytes, &r)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	return r.Data.List, nil
}

func announceChallenges(challenges []database.Active) []error {
	var messages []string
	for _, c := range challenges {
		msg := fmt.Sprintf(
			"A new %s challenge \"%s\" worth %g points has been released!",
			c.Challenge.Category,
			c.Challenge.Name,
			c.Points,
		)
		messages = append(messages, msg)
	}
	if len(messages) < 1 {
		return nil
	}
	return discord.SendAnnounce(messages)
}

type challengeSolve struct {
	TeamName      string
	ChallengeName string
	ChallengeID   string
}

func (bot *Bot) queryNewSolves() ([]challengeSolve, error) {
	const newSolvesQueryFmtStr = `query {
		challenges_solves(
			order_by: {challenge_id: asc, time: asc},
			where: {
				event: {environments: {name: {_eq: %+q}}},
				challenge_id: {_nin: %+q},
				team: {
					team_settings: {visible: {_eq: true}}
				}
			}
		) {
			event_id
			challenge {
				challenge {
					name
					id
					category
				}
			}
			team {
				name
			}
			time
		}
	}`

	// Construct a query excluding challenge IDs we already announced
	query := fmt.Sprintf(
		newSolvesQueryFmtStr,
		bot.Config.Event.Environment,
		bot.AnnouncedFirstSolve,
	)

	responseBytes, err := database.GQLQuery(query, nil, bot.Config.Database.DefaultQueryRole)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	r := struct {
		Data struct {
			List []database.ChallengeSolves `json:"challenges_solves"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(responseBytes, &r)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	solves := []challengeSolve{}
	for _, entry := range r.Data.List {
		if util.StrListContains(bot.AnnouncedFirstSolve, entry.Active.Challenge.ID) {
			logger.Debugf("Challenge already solved %s", entry.Active.Challenge.Name)
			continue
		}
		solves = append(solves, challengeSolve{
			TeamName:      entry.Team.Name,
			ChallengeName: entry.Active.Challenge.Name,
			ChallengeID:   entry.Active.Challenge.ID,
		})
		bot.AnnouncedFirstSolve = append(bot.AnnouncedFirstSolve, entry.Active.Challenge.ID)
	}
	return solves, nil
}

func (bot *Bot) announceSolves(solves []challengeSolve) []error {
	var messages []string
	for _, s := range solves {
		msg := fmt.Sprintf(
			"%s was the first to solve \"%s\"!",
			s.TeamName,
			s.ChallengeName,
		)
		messages = append(messages, msg)
	}
	if len(messages) < 1 {
		return nil
	}
	errs := discord.SendAnnounce(messages)
	if errs != nil {
		// Remove the ID since the announcement failed
		for _, s := range solves {
			bot.AnnouncedFirstSolve = util.StrListDelete(
				bot.AnnouncedFirstSolve, s.ChallengeID)
		}
		return errs
	}
	return nil
}

// Tick triggers every tick
func (bot *Bot) Tick() int {
	challenges, err := bot.queryReleasedChallenges(*bot.LatestChallengeTime)
	if err != nil {
		logger.Error(err)
		errs := discord.SendStaff("error querying released challenges")
		if errs != nil {
			util.LogErrors(logger, errs)
		}
	} else {
		errs := announceChallenges(challenges)
		if errs != nil {
			util.LogErrors(logger, errs)
			errs = discord.SendStaff("error announcing challenges")
			if errs != nil {
				util.LogErrors(logger, errs)
			}
		} else {
			// If there is at least 1 entry, use the latest as the next query
			if len(challenges) > 0 {
				// Not sure if holding onto this reference will keep the whole struct
				// from being garbage collected, so just make a new variable
				copy := challenges[0].ReleaseTime
				bot.LatestChallengeTime = &copy
			}
		}

		res, err := PrettyStruct(challenges)
		if err != nil {
			logger.Error(err)
			return 1
		}
		fmt.Println(res)
	}

	solves, err := bot.queryNewSolves()
	if err != nil {
		logger.Error(err)
		errs := discord.SendStaff("error querying new solves")
		if errs != nil {
			util.LogErrors(logger, errs)
		}
	} else {
		errs := bot.announceSolves(solves)
		if errs != nil {
			util.LogErrors(logger, errs)
			errs = discord.SendStaff("error announcing solves")
			if errs != nil {
				util.LogErrors(logger, errs)
			}
		}
	}
	return 0
}

const envQuery = `query EnvSettings {
	environments(where: {name: {_eq: %+q}}) {
		settings {
			keycloak_url
			keycloak_realm
		}
	}
}`

// setupFromEnvironment Query the database for extra configuration settings
func (bot *Bot) setupFromEnvironment() error {
	query := fmt.Sprintf(
		envQuery,
		bot.Config.Event.Environment,
	)
	responseBytes, err := database.GQLQuery(query, nil, bot.Config.Database.UnauthenticatedRole)
	if err != nil {
		logger.Error(err)
		return err
	}
	r := struct {
		Data struct {
			Environments []struct {
				Settings struct {
					KeycloakURL   string `json:"keycloak_url"`
					KeycloakRealm string `json:"keycloak_realm"`
				} `json:"settings"`
			} `json:"environments"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(responseBytes, &r)
	if err != nil {
		logger.Error(err)
		return err
	}
	if 0 == len(r.Data.Environments) {
		logger.Panicf("No environments found for %s", bot.Config.Event.Environment)
	}
	bot.Config.Authentication.SetupKeycloakURL(
		r.Data.Environments[0].Settings.KeycloakURL,
		r.Data.Environments[0].Settings.KeycloakRealm,
	)
	logger.Debugf("openid-configuration URL: %s", bot.Config.Authentication.OIDCCfgURL)
	return nil
}

// Setup the bot
func (bot *Bot) Setup() {
	if bot.LatestChallengeTime == nil {
		now := time.Now().UTC()
		bot.LatestChallengeTime = &now
	}
	//Setup database
	database.SetConfig(&bot.Config.Database)
	//Setup auth from GQL
	bot.setupFromEnvironment()
	//Setup auth
	err := authentication.SetupOIDC(&bot.Config.Authentication)
	if err != nil {
		logger.Fatal(err)
	}
	//Setup discord
	err = discord.Setup(&bot.Config.Discord)
	if err != nil {
		logger.Fatal(err)
	}
}

// Cleanup the bot
func (bot *Bot) Cleanup() {
	discord.Cleanup()
}
