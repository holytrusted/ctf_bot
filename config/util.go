package config

import "fmt"

// SetupKeycloakURL construct and set the well-known url for keycloak OIDC
func (c *AuthConfig) SetupKeycloakURL(baseURL string, realm string) {
	c.OIDCCfgURL = fmt.Sprintf(
		`%s/realms/%s/.well-known/openid-configuration`,
		baseURL,
		realm,
	)
}
