package config

import "time"

// DiscordConfig - Discord related variables and secrets
type DiscordConfig struct {
	Token            *string  `mapstructure:"token"` // Discord token for auth
	StaffChannels    []string `mapstructure:"staff_channels"`
	AnnounceChannels []string `mapstructure:"announce_channels"`
}

// RuntimeConfig - Runtime config variables for ctf_bot
type RuntimeConfig struct {
	Path               string        `mapstructure:"path"` // Runtime config location or address (redis)
	AnnounceChallenges bool          `mapstructure:"announce_challenges"`
	AnnounceFirstSolve bool          `mapstructure:"announce_first_solve"`
	CheckInterval      time.Duration `mapstructure:"check_interval"`
}

// LogConfig - Log variables
type LogConfig struct {
	Level string `mapstructure:"level"`
}

// DatabaseConfig - Hasura connection information
type DatabaseConfig struct {
	GqlURI              string        `mapstructure:"gql_uri"`
	DefaultQueryRole    string        `mapstructure:"default_query_role"`
	TLSSkipVerify       bool          `mapstructure:"tls_skip_verify"`
	RequestTimeout      time.Duration `mapstructure:"request_timeout"`
	UnauthenticatedRole string        `mapstructure:"unauthenticated_role"`
}

// AuthConfig - keycloak authentication variables
type AuthConfig struct {
	OIDCCfgURL       string `mapstructure:"oidc_cfg_url"`
	OIDCClientID     string `mapstructure:"oidc_client_id"`
	OIDCClientSecret string `mapstructure:"oidc_client_secret"`
}

// EventConfig - stores event related data.
type EventConfig struct {
	Environment string `mapstructure:"environment"`
}

// BotConfig - rootlevel configuration for ctf_bot
type BotConfig struct {
	Discord        DiscordConfig  `mapstructure:"discord"`
	Runtime        RuntimeConfig  `mapstructure:"runtime"`
	Log            LogConfig      `mapstructure:"log"`
	Database       DatabaseConfig `mapstructure:"database"`
	Authentication AuthConfig     `mapstructure:"authentication"`
	Event          EventConfig    `mapstructure:"event"`
}
